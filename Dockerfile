FROM python:3-slim

ADD src/ /
ENV RABBIT_HOST="localhost"

VOLUME upload

EXPOSE 8888

RUN pip install tornado pika pyyaml==5.3.0

ENTRYPOINT ["python", "app.py"]
